Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header date is : 
Thu, 07 Mar 2024 13:45:31 GMT

Response body is : 
{"name":"morpheus","job":"leader","id":"341","createdAt":"2024-03-07T13:45:31.696Z"}